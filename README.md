[![pipeline status](https://gitlab.inria.fr/mpoquet/nix-tutorial/badges/master/pipeline.svg)](https://gitlab.inria.fr/mpoquet/nix-tutorial/pipelines)
[![pages](https://img.shields.io/badge/pages-online-green.svg)](http://mpoquet.gitlabpages.inria.fr/nix-tutorial/)

Nix tutorial
============

This repository contains material to build the Nix tutorial hosted
[here](http://mpoquet.gitlabpages.inria.fr/nix-tutorial).
