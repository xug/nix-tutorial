Resources to Go Further
=======================

#. `Nix pills`_ are Nix official tutorials.
#. Exploring `Nixpkgs`_ functions and packages can be a great source of inspiration.
#. Exploring simpler package repositories such as kapack_ can also be a good source of inspiration.

Otherwise, here is random list of Nix-related resources.

- Lorri_ is a nix-shell replacement that tries to maximize comfort while developing projects.
- remoteshell_ can be used to simply use nix-shell on remote machines.
- `NixOS wiki on pinning Nixpkgs`_
- `NixOS wiki on customizing packages`_
- `Nix file naming conventions`_
- `Information about channels`_
- `Channels and reproducibility`_

.. _Nix pills: https://nixos.org/nixos/nix-pills/
.. _Nixpkgs: https://github.com/NixOS/nixpkgs
.. _kapack: https://github.com/oar-team/kapack/
.. _Lorri: https://github.com/target/lorri
.. _remoteshell: https://freux.fr/orgposts/remoteshell.html
.. _NixOS wiki on pinning Nixpkgs: https://nixos.wiki/wiki/FAQ/Pinning_Nixpkgs
.. _Nix file naming conventions: https://stackoverflow.com/questions/44088192/when-and-how-should-default-nix-shell-nix-and-release-nix-be-used
.. _Channels and reproducibility: https://matrix.ai/blog/intro-to-nix-channels-and-reproducible-nixos-environment/
.. _Information about channels: https://matthewbauer.us/blog/channel-changing.html
.. _Writing Nix expression within orgmode: https://matthewbauer.us/blog/nix-and-org.html
.. _NixOS wiki on customizing packages: https://nixos.org/nixos/manual/index.html#sec-customising-packages
